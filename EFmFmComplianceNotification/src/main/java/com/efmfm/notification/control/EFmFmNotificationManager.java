package com.efmfm.notification.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.efmfm.notification.config.EFmFmJobLauncher;
import com.efmfm.notification.config.EFmFmPropertiesConfig;
import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.service.EFmFmNotificationService;
import com.efmfm.notification.util.CommonUtil;
import com.efmfm.notification.util.CommonUtil.AuditType;
import com.efmfm.notification.util.CommonUtil.ComplianceType;
import com.efmfm.notification.util.CommonUtil.ContactType;
import com.efmfm.notification.util.CommonUtil.NotificationStatus;
import com.efmfm.notification.util.CommonUtil.NotificationType;
import com.efmfm.notification.util.MailNotificationUtil;
import com.efmfm.notification.util.SMSNotificationUtil;

@Component
public class EFmFmNotificationManager {
	private static final Logger log = LoggerFactory.getLogger(EFmFmJobLauncher.class);
	
	@Autowired
	EFmFmNotificationService notificationService;
	
	@Autowired
	MailNotificationUtil mailNotification;
	
	@Autowired
	EFmFmPropertiesConfig propertiesConfig;
	
	public void triggerNotificationJob() {
		  List<Object[]> clientBranchList = notificationService.getAllBranchDetails(propertiesConfig.getComplianceBranchConfig(), CommonUtil.getSplitedList(propertiesConfig.getCombinedFacility()));
		  executeDriverCompliance(clientBranchList);
		  executeVehicleCompliance(clientBranchList);
		  executeVendorCompliance();
	}

	private void executeDriverCompliance(List<Object[]> clientBranchList) {
		for(Object[] clientBranch : clientBranchList){
			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;
			
			int licenseExpiryDay = clientBranch[3] != null ? (int) clientBranch[3] : 0;
			int licenseRepeatAlertsDay = clientBranch[4] != null ? (int) clientBranch[4] : 0;
			String licenseEmailId = clientBranch[5] != null ? (String) clientBranch[5] : "";
			String licenseNotifiyType = clientBranch[6] != null ? (String) clientBranch[6] : "";
			String licenseSMSNumber = clientBranch[7] != null ? (String) clientBranch[7] : "";    		 
    		 
			int medicalExpiryDay = clientBranch[8] != null ? (int) clientBranch[8] : 0;
			int medicalRepeatAlertsDay = clientBranch[9] != null ? (int) clientBranch[9] : 0;
			String medicalEmailId = clientBranch[10] != null ? (String) clientBranch[10] : "";
			String medicalNotifiyType = clientBranch[11] != null ? (String) clientBranch[11] : "";
			String medicalSMSNumber = clientBranch[12] != null ? (String) clientBranch[12] : ""; 
    		 
			int policeVerifyExpiryDay = clientBranch[13] != null ? (int) clientBranch[13] : 0;
			int policeVerifyRepeatAlertsDay = clientBranch[14] != null ? (int) clientBranch[14] : 0;
			String policeVerifyEmailId = clientBranch[15] != null ? (String) clientBranch[15] : "";
			String policeVerifyNotifiyType = clientBranch[16] != null ? (String) clientBranch[16] : "";
			String policeVerifySMSNumber = clientBranch[17] != null ? (String) clientBranch[17] : "";    		 
    		 
			int ddTrainingExpiryDay = clientBranch[18] != null ? (int) clientBranch[18] : 0;
			int ddTrainingRepeatAlertsDay = clientBranch[19] != null ? (int) clientBranch[19] : 0;
			String ddTrainingEmailId = clientBranch[20] != null ? (String) clientBranch[20] : "";
			String ddTrainingNotifiyType = clientBranch[21] != null ? (String) clientBranch[21] : "";
			String ddTrainingSMSNumber = clientBranch[22] != null ? (String) clientBranch[22] : "";    		 
    		
			String currentTimeZone= clientBranch[53] != null ? (String) clientBranch[53] : "";
			
			 String[] mailDetails=new String[7];
			 mailDetails[0]=clientBranch[54]!=null?clientBranch[54].toString():"";//EmailUserName
			 mailDetails[1]=clientBranch[55]!=null?clientBranch[55].toString():"";//EmailPassword
			 mailDetails[2]=clientBranch[56]!=null?clientBranch[56].toString():"";//EmailHost
			 mailDetails[3]=clientBranch[57]!=null?clientBranch[57].toString():"";//EmailAuth
			 mailDetails[4]=clientBranch[58]!=null?clientBranch[58].toString():"";//EmailSecureSSLORTLS
			 mailDetails[5]=clientBranch[59]!=null?clientBranch[59].toString():"";//EmailPort

			if(currentTimeZone != null){
				Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
				if(licenseExpiryDay != 0 && !"NONE".equalsIgnoreCase(licenseNotifiyType)){
					processNotification(branchId, currentDate, licenseExpiryDay, licenseRepeatAlertsDay, licenseEmailId, licenseNotifiyType, licenseSMSNumber, 
							ComplianceType.DRIVER,  NotificationType.LICENSE_EXPIRE, propertiesConfig.getDriverLicenseValid(), null, mailDetails);
				}
				if(medicalExpiryDay != 0 && !"NONE".equalsIgnoreCase(medicalNotifiyType)){
					processNotification(branchId, currentDate, medicalExpiryDay, medicalRepeatAlertsDay, medicalEmailId, medicalNotifiyType, medicalSMSNumber, 
							ComplianceType.DRIVER,  NotificationType.MEDICAL_FITNESS, propertiesConfig.getDriverMedicalFitnessValid(), null, mailDetails);
				}
				if(policeVerifyExpiryDay != 0 && !"NONE".equalsIgnoreCase(policeVerifyNotifiyType)){
					processNotification(branchId, currentDate, policeVerifyExpiryDay, policeVerifyRepeatAlertsDay, policeVerifyEmailId, policeVerifyNotifiyType, policeVerifySMSNumber, 
							ComplianceType.DRIVER,  NotificationType.POLICE_VERIFICATION, propertiesConfig.getDriverPoliceValid(), null, mailDetails);
				}
				if(ddTrainingExpiryDay != 0 && !"NONE".equalsIgnoreCase(ddTrainingNotifiyType)){
					processNotification(branchId, currentDate, ddTrainingExpiryDay, ddTrainingRepeatAlertsDay, ddTrainingEmailId, ddTrainingNotifiyType, ddTrainingSMSNumber, 
							ComplianceType.DRIVER,  NotificationType.DD_TRAINING, propertiesConfig.getDriverDdTraningValid(), null, mailDetails);
				}
			}
				 
		}
	}
	private void executeVehicleCompliance(List<Object[]> clientBranchList) {
		for (Object[] clientBranch : clientBranchList) {
			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;
			
			int pollutionDueExpiryDay = clientBranch[23] != null ? (int) clientBranch[23] : 0;
			int pollutionDueRepeatAlertsDay = clientBranch[24] != null ? (int) clientBranch[24] : 0;
			String pollutionDueEmailId = clientBranch[25] != null ? (String) clientBranch[25] : "";
			String pollutionDueNotifiyType = clientBranch[26] != null ? (String) clientBranch[26] : "";
			String pollutionDueSMSNumber = clientBranch[27] != null ? (String) clientBranch[27] : "";

			int isuranceDueExpiryDay = clientBranch[28] != null ? (int) clientBranch[28] : 0;
			int insuranceDueRepeatAlertsDay = clientBranch[29] != null ? (int) clientBranch[29] : 0;
			String insuranceDueEmailId = clientBranch[30] != null ? (String) clientBranch[30] : "";
			String insuranceDueNotifiyType = clientBranch[31] != null ? (String) clientBranch[31] : "";
			String insuranceDueSMSNumber = clientBranch[32] != null ? (String) clientBranch[32] : "";

			int taxCertificateExpiryDay = clientBranch[33] != null ? (int) clientBranch[33] : 0;
			int taxCertificateRepeatAlertsDay = clientBranch[34] != null ? (int) clientBranch[34] : 0;
			String taxCertificateEmailId = clientBranch[35] != null ? (String) clientBranch[35] : "";
			String taxCertificateNotifiyType = clientBranch[36] != null ? (String) clientBranch[36] : "";
			String taxCertificateSMSNumber = clientBranch[37] != null ? (String) clientBranch[37] : "";

			int statePermitDueExpiryDay = clientBranch[38] != null ? (int) clientBranch[38] : 0;
			int statePermitDueRepeatAlertsDay = clientBranch[39] != null ? (int) clientBranch[39] : 0;
			String statePermitDueEmailId = clientBranch[40] != null ? (String) clientBranch[40] : "";
			String statePermitDueNotifiyType = clientBranch[41] != null ? (String) clientBranch[41] : "";
			String statePermitDueSMSNumber = clientBranch[42] != null ? (String) clientBranch[42] : "";
			
			int natnlPermitDueExpiryDay = clientBranch[43] != null ? (int) clientBranch[43] : 0;
			int natnlPermitDueRepeatAlertsDay = clientBranch[44] != null ? (int) clientBranch[44] : 0;
			String natnlPermitDueEmailId = clientBranch[45] != null ? (String) clientBranch[45] : "";
			String natnlPermitDueNotifiyType = clientBranch[46] != null ? (String) clientBranch[46] : "";
			String natnlPermitDueSMSNumber = clientBranch[47] != null ? (String) clientBranch[47] : "";

			// Here vehicle maintenance will be considered as vehicle fitness at
			// client branchPO Table
			int vehicelMaintenanceExpiryDay = clientBranch[48] != null ? (int) clientBranch[48] : 0;
			int vehicelMaintenanceRepeatAlertsDay = clientBranch[49] != null ? (int) clientBranch[49] : 0;
			String vehicelMaintenanceEmailId = clientBranch[50] != null ? (String) clientBranch[50] : "";
			String vehicelMaintenanceNotifiyType = clientBranch[51] != null ? (String) clientBranch[51] : "";
			String vehicelMaintenanceSMSNumber = clientBranch[52] != null ? (String) clientBranch[52] : "";

			String currentTimeZone= clientBranch[53] != null ? (String) clientBranch[53] : "";
			
			 String[] mailDetails=new String[7];
			 mailDetails[0]=clientBranch[54]!=null?clientBranch[54].toString():"";
			 mailDetails[1]=clientBranch[55]!=null?clientBranch[55].toString():"";
			 mailDetails[2]=clientBranch[56]!=null?clientBranch[56].toString():"";
			 mailDetails[3]=clientBranch[57]!=null?clientBranch[57].toString():"";
			 mailDetails[4]=clientBranch[58]!=null?clientBranch[58].toString():"";//ssl
			 mailDetails[5]=clientBranch[59]!=null?clientBranch[59].toString():"";//port
			 
			if(currentTimeZone != null){
				Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
				if(pollutionDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(pollutionDueNotifiyType)){
					processNotification(branchId, currentDate, pollutionDueExpiryDay, pollutionDueRepeatAlertsDay, pollutionDueEmailId, pollutionDueNotifiyType, pollutionDueSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.POLLUTION_DUE, propertiesConfig.getVehiclePollutionValid(), null, mailDetails);
				}
				if(isuranceDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(insuranceDueNotifiyType)){
					processNotification(branchId, currentDate, isuranceDueExpiryDay, insuranceDueRepeatAlertsDay, insuranceDueEmailId, insuranceDueNotifiyType, insuranceDueSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.INSURANCE_DUE, propertiesConfig.getVehicleInsuranceValid(), null, mailDetails);
				}
				if(taxCertificateExpiryDay != 0 && !"NONE".equalsIgnoreCase(taxCertificateNotifiyType)){
					processNotification(branchId, currentDate, taxCertificateExpiryDay, taxCertificateRepeatAlertsDay, taxCertificateEmailId, taxCertificateNotifiyType, taxCertificateSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.TAX_DUE, propertiesConfig.getVehicleTaxValid(), null, mailDetails);
				}
				if(statePermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(statePermitDueNotifiyType)){
					processNotification(branchId, currentDate, statePermitDueExpiryDay, statePermitDueRepeatAlertsDay, statePermitDueEmailId, statePermitDueNotifiyType, statePermitDueSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.STATE_PERMIT_DUE, propertiesConfig.getVehicleStateValid(), null, mailDetails);
				}
				if(natnlPermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(natnlPermitDueNotifiyType)){
					processNotification(branchId, currentDate, natnlPermitDueExpiryDay, natnlPermitDueRepeatAlertsDay, natnlPermitDueEmailId, natnlPermitDueNotifiyType, natnlPermitDueSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.NATIONAL_PERMIT_DUE, propertiesConfig.getVehicleNationalValid(), null, mailDetails);
				}
				if(vehicelMaintenanceExpiryDay != 0 && !"NONE".equalsIgnoreCase(vehicelMaintenanceNotifiyType)){
					processNotification(branchId, currentDate, vehicelMaintenanceExpiryDay, vehicelMaintenanceRepeatAlertsDay, vehicelMaintenanceEmailId, vehicelMaintenanceNotifiyType, vehicelMaintenanceSMSNumber, 
							ComplianceType.VEHICLE,  NotificationType.VEHICLE_FITNESS, propertiesConfig.getVehicleFitnessValid(), null, mailDetails);
				}
			}
				 
		}
	}
	
	private void executeVendorCompliance() {
		 List<Object[]> clientBranchList = notificationService.getAllBranchDetails(propertiesConfig.getVendorCompConfigBranch(), CommonUtil.getSplitedList(propertiesConfig.getCombinedFacility()));
		 for(Object[] clientBranch : clientBranchList){
			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;
			
			int licenseExpiryDay = clientBranch[1] != null ? (int) clientBranch[1] : 0;
			int licenseRepeatAlertsDay = clientBranch[2] != null ? (int) clientBranch[2] : 0;
			String licenseEmailId = clientBranch[3] != null ? (String) clientBranch[3] : "";
			String licenseNotifiyType = clientBranch[4] != null ? (String) clientBranch[4] : "";
			String licenseSMSNumber = clientBranch[5] != null ? (String) clientBranch[5] : "";

			int medicalExpiryDay = clientBranch[6] != null ? (int) clientBranch[6] : 0;
			int medicalRepeatAlertsDay = clientBranch[7] != null ? (int) clientBranch[7] : 0;
			String medicalEmailId = clientBranch[8] != null ? (String) clientBranch[8] : "";
			String medicalNotifiyType = clientBranch[9] != null ? (String) clientBranch[9] : "";
			String medicalSMSNumber = clientBranch[10] != null ? (String) clientBranch[10] : "";

			int policeVerifyExpiryDay = clientBranch[11] != null ? (int) clientBranch[11] : 0;
			int policeVerifyRepeatAlertsDay = clientBranch[12] != null ? (int) clientBranch[12] : 0;
			String policeVerifyEmailId = clientBranch[13] != null ? (String) clientBranch[13] : "";
			String policeVerifyNotifiyType = clientBranch[14] != null ? (String) clientBranch[14] : "";
			String policeVerifySMSNumber = clientBranch[15] != null ? (String) clientBranch[15] : "";

			int ddTrainingExpiryDay = clientBranch[16] != null ? (int) clientBranch[16] : 0;
			int ddTrainingRepeatAlertsDay = clientBranch[17] != null ? (int) clientBranch[17] : 0;
			String ddTrainingEmailId = clientBranch[18] != null ? (String) clientBranch[18] : "";
			String ddTrainingNotifiyType = clientBranch[19] != null ? (String) clientBranch[19] : "";
			String ddTrainingSMSNumber = clientBranch[20] != null ? (String) clientBranch[20] : "";
			
			int pollutionDueExpiryDay = clientBranch[21] != null ? (int) clientBranch[21] : 0;
			int pollutionDueRepeatAlertsDay = clientBranch[22] != null ? (int) clientBranch[22] : 0;
			String pollutionDueEmailId = clientBranch[23] != null ? (String) clientBranch[23] : "";
			String pollutionDueNotifiyType = clientBranch[24] != null ? (String) clientBranch[24] : "";
			String pollutionDueSMSNumber = clientBranch[25] != null ? (String) clientBranch[25] : "";

			int isuranceDueExpiryDay = clientBranch[26] != null ? (int) clientBranch[26] : 0;
			int insuranceDueRepeatAlertsDay = clientBranch[27] != null ? (int) clientBranch[27] : 0;
			String insuranceDueEmailId = clientBranch[28] != null ? (String) clientBranch[28] : "";
			String insuranceDueNotifiyType = clientBranch[29] != null ? (String) clientBranch[29] : "";
			String insuranceDueSMSNumber = clientBranch[30] != null ? (String) clientBranch[30] : "";

			int taxCertificateExpiryDay = clientBranch[31] != null ? (int) clientBranch[31] : 0;
			int taxCertificateRepeatAlertsDay = clientBranch[32] != null ? (int) clientBranch[32] : 0;
			String taxCertificateEmailId = clientBranch[33] != null ? (String) clientBranch[35] : "";
			String taxCertificateNotifiyType = clientBranch[34] != null ? (String) clientBranch[34] : "";
			String taxCertificateSMSNumber = clientBranch[35] != null ? (String) clientBranch[35] : "";

			int statePermitDueExpiryDay = clientBranch[36] != null ? (int) clientBranch[36] : 0;
			int statePermitDueRepeatAlertsDay = clientBranch[37] != null ? (int) clientBranch[37] : 0;
			String statePermitDueEmailId = clientBranch[38] != null ? (String) clientBranch[38] : "";
			String statePermitDueNotifiyType = clientBranch[39] != null ? (String) clientBranch[39] : "";
			String statePermitDueSMSNumber = clientBranch[40] != null ? (String) clientBranch[40] : "";
			
			int natnlPermitDueExpiryDay = clientBranch[41] != null ? (int) clientBranch[41] : 0;
			int natnlPermitDueRepeatAlertsDay = clientBranch[42] != null ? (int) clientBranch[42] : 0;
			String natnlPermitDueEmailId = clientBranch[43] != null ? (String) clientBranch[43] : "";
			String natnlPermitDueNotifiyType = clientBranch[44] != null ? (String) clientBranch[44] : "";
			String natnlPermitDueSMSNumber = clientBranch[45] != null ? (String) clientBranch[45] : "";


			// Here vehicle maintenance will be considered as vehicle fitness at
			// client branchPO Table
			int vehicelFitnessExpiryDay = clientBranch[46] != null ? (int) clientBranch[46] : 0;
			int vehicelFitnessRepeatAlertsDay = clientBranch[47] != null ? (int) clientBranch[47] : 0;
			String vehicelFitnessEmailId = clientBranch[48] != null ? (String) clientBranch[48] : "";
			String vehicelFitnessNotifiyType = clientBranch[49] != null ? (String) clientBranch[49] : "";
			String vehicelFitnessSMSNumber = clientBranch[50] != null ? (String) clientBranch[50] : "";
			
			String currentTimeZone= clientBranch[51] != null ? (String) clientBranch[51] : "";
			
			 String[] mailDetails=new String[7];
			 mailDetails[0]=clientBranch[52]!=null?clientBranch[52].toString():"";
			 mailDetails[1]=clientBranch[53]!=null?clientBranch[53].toString():"";
			 mailDetails[2]=clientBranch[54]!=null?clientBranch[54].toString():"";
			 mailDetails[3]=clientBranch[55]!=null?clientBranch[55].toString():"";
			 mailDetails[4]=clientBranch[56]!=null?clientBranch[56].toString():"";//ssl
			 mailDetails[5]=clientBranch[57]!=null?clientBranch[57].toString():"";//port
			 		 	 
			
			List<Object[]> vendorList = notificationService.getVendorDetailsByBranchId(propertiesConfig.getUniqueVendor(), branchId);
			for(Object[] vendorDetail : vendorList){
				if(currentTimeZone != null){
					Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
					if(licenseExpiryDay != 0 && !"NONE".equalsIgnoreCase(licenseNotifiyType)){
						processNotification(branchId, currentDate, licenseExpiryDay, licenseRepeatAlertsDay, licenseEmailId, licenseNotifiyType, licenseSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.LICENSE_EXPIRE, propertiesConfig.getVendorLicenseValid(), vendorDetail, mailDetails);
					}
					if(medicalExpiryDay != 0 && !"NONE".equalsIgnoreCase(medicalNotifiyType)){
						processNotification(branchId, currentDate, medicalExpiryDay, medicalRepeatAlertsDay, medicalEmailId, medicalNotifiyType, medicalSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.MEDICAL_FITNESS, propertiesConfig.getVendorMedicalFitnessValid(), vendorDetail, mailDetails);
					}
					if(policeVerifyExpiryDay != 0 && !"NONE".equalsIgnoreCase(policeVerifyNotifiyType)){
						processNotification(branchId, currentDate, policeVerifyExpiryDay, policeVerifyRepeatAlertsDay, policeVerifyEmailId, policeVerifyNotifiyType, policeVerifySMSNumber, 
								ComplianceType.VENDOR,  NotificationType.POLICE_VERIFICATION, propertiesConfig.getVendorPoliceValid(), vendorDetail, mailDetails);
					}
					if(ddTrainingExpiryDay != 0 && !"NONE".equalsIgnoreCase(ddTrainingNotifiyType)){
						processNotification(branchId, currentDate, ddTrainingExpiryDay, ddTrainingRepeatAlertsDay, ddTrainingEmailId, ddTrainingNotifiyType, ddTrainingSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.DD_TRAINING, propertiesConfig.getVendorDdTraningValid(), vendorDetail, mailDetails);
					}
					if(pollutionDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(pollutionDueNotifiyType)){
						processNotification(branchId, currentDate, pollutionDueExpiryDay, pollutionDueRepeatAlertsDay, pollutionDueEmailId, pollutionDueNotifiyType, pollutionDueSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.POLLUTION_DUE, propertiesConfig.getVendorPollutionValid(), vendorDetail, mailDetails);
					}
					if(isuranceDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(insuranceDueNotifiyType)){
						processNotification(branchId, currentDate, isuranceDueExpiryDay, insuranceDueRepeatAlertsDay, insuranceDueEmailId, insuranceDueNotifiyType, insuranceDueSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.INSURANCE_DUE, propertiesConfig.getVendorInsuranceValid(), vendorDetail, mailDetails);
					}
					if(taxCertificateExpiryDay != 0 && !"NONE".equalsIgnoreCase(taxCertificateNotifiyType)){
						processNotification(branchId, currentDate, taxCertificateExpiryDay, taxCertificateRepeatAlertsDay, taxCertificateEmailId, taxCertificateNotifiyType, taxCertificateSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.TAX_DUE, propertiesConfig.getVendorTaxValid(), vendorDetail, mailDetails);
					}
					if(statePermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(statePermitDueNotifiyType)){
						processNotification(branchId, currentDate, statePermitDueExpiryDay, statePermitDueRepeatAlertsDay, statePermitDueEmailId, statePermitDueNotifiyType, statePermitDueSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.STATE_PERMIT_DUE, propertiesConfig.getVendorStatePermitValid(), vendorDetail, mailDetails);
					}
					if(natnlPermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(natnlPermitDueNotifiyType)){
						processNotification(branchId, currentDate, natnlPermitDueExpiryDay, natnlPermitDueRepeatAlertsDay, natnlPermitDueEmailId, natnlPermitDueNotifiyType, natnlPermitDueSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.NATIONAL_PERMIT_DUE, propertiesConfig.getVendorNationalPermitValid(), vendorDetail, mailDetails);
					}
					if(vehicelFitnessExpiryDay != 0 && !"NONE".equalsIgnoreCase(vehicelFitnessNotifiyType)){
						processNotification(branchId, currentDate, vehicelFitnessExpiryDay, vehicelFitnessRepeatAlertsDay, vehicelFitnessEmailId, vehicelFitnessNotifiyType, vehicelFitnessSMSNumber, 
								ComplianceType.VENDOR,  NotificationType.VEHICLE_FITNESS, propertiesConfig.getVendorFitnessValid(), vendorDetail, mailDetails);
					}
				}
			}
			

		 }
	}
	@SuppressWarnings("unlikely-arg-type")
	private void processNotification(int branchId, Date currentDate, int expiryDay, int repeatAlert,
			String emailId, String requestType, String smsNumber, ComplianceType compType, NotificationType notifyType,
			String sqlQuery, Object[] vendorDetail, String[] mailDetails) {
		log.info("processNotification==>" + sqlQuery);
		AuditType auditType = null;
		int primaryId = 0;
		String emailMsg = "";
		String smsMsg = "";
		if(ComplianceType.DRIVER.equals(compType) || ComplianceType.VEHICLE.equals(compType)){
			auditType = AuditType.INTERNAL;
			primaryId = branchId;
			smsMsg = propertiesConfig.getTransportVehicleDriverSms().replace("$complianceType$", 
					ComplianceType.DRIVER.equals(compType) ? "Driver" : "Vehicle")
					.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");
			emailMsg = propertiesConfig.getTransportVehicleDriverEmail().replace("$complianceType$",
					ComplianceType.DRIVER.equals(compType) ? "Driver" : "Vehicle")
					.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");
		}else{
			auditType = AuditType.VENDOR;
			if(vendorDetail != null){
				primaryId = (int) vendorDetail[0];
				Set<String> emailIds  = getVendorContactDetailsByType(vendorDetail, ContactType.EMAIL);
				Set<String> mobileNumbers = getVendorContactDetailsByType(vendorDetail, ContactType.SMS);
				smsNumber = String.join(",", mobileNumbers);
				emailId = String.join(",", emailIds);
				smsMsg = propertiesConfig.getTransportVendorSms().replace("$vendorName$", 
						(String) vendorDetail[1]).replace("$complianceType$",
						("Driver".equalsIgnoreCase(notifyType.getType()) ?  "Driver" : "Vehicle"))
						.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");
				emailMsg = propertiesConfig.getTransportVendorEmail().replace("$vendorName$", 
						(String) vendorDetail[1]).replace("$complianceType$",
						("Driver".equalsIgnoreCase(notifyType.getType()) ?  "Driver" : "Vehicle"))
						.replace("$logisticType$","Driver".equalsIgnoreCase(notifyType.getType()) ? "Operation" : "Commercial")
						.replace("$complianceTypes$","Driver".equalsIgnoreCase(notifyType.getType()) ? "driver/drivers" : "cab/cabs")
						.replace("$temp$","Driver".equalsIgnoreCase(notifyType.getType()) ? "work" : "ply")
						.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");
			}
		}
		boolean isTriggerNotification = false;
		String lasExecuteQuery = ComplianceType.VENDOR.equals(compType) ? propertiesConfig.getLastExecutionVendor() : 
			propertiesConfig.getLastExecution();
		List<Object[]> lastExecutionList = notificationService.getLastExecutionByNotifyType(lasExecuteQuery, branchId, auditType.name(),
				compType.name(), notifyType.name(), primaryId);
		if(!lastExecutionList.isEmpty())
			isTriggerNotification = CommonUtil.isTriggerNotification(lastExecutionList, repeatAlert, currentDate);
		else
			isTriggerNotification = true;
		if(isTriggerNotification){
			Map<String, Object> notifyContent = getNotificationContentIfExist(sqlQuery, primaryId, currentDate, expiryDay, compType, notifyType);
			if(!notifyContent.isEmpty()){
				notifyContent.put("smsContent", smsMsg);
				notifyContent.put("emailContent", emailMsg);
				triggerNotification(branchId, requestType, emailId, smsNumber, auditType, compType, notifyType, notifyContent, primaryId, mailDetails);
			}
		}
	}

	private Set<String> getVendorContactDetailsByType(Object[] vendorDetail, ContactType contactType) {
		Set<String> contact = new HashSet<String>();
		if(ContactType.SMS.equals(contactType)){
			if(!StringUtils.isBlank((String) vendorDetail[3]))
				contact.add((String) vendorDetail[3]);
			if(!StringUtils.isBlank((String) vendorDetail[5]))
				contact.add((String) vendorDetail[5]);
			if(!StringUtils.isBlank((String) vendorDetail[6]))
				contact.add((String) vendorDetail[6]);
			if(!StringUtils.isBlank((String) vendorDetail[7]))
				contact.add((String) vendorDetail[7]);
			if(!StringUtils.isBlank((String) vendorDetail[8]))
				contact.add((String) vendorDetail[8]);
			if(!StringUtils.isBlank((String) vendorDetail[9]))
				contact.add((String) vendorDetail[9]);
		}else if(ContactType.EMAIL.equals(contactType)){
			if(!StringUtils.isBlank((String) vendorDetail[4]))
				contact.add((String) vendorDetail[4]);
			if(!StringUtils.isBlank((String) vendorDetail[11]))
				contact.add((String) vendorDetail[11]);
			if(!StringUtils.isBlank((String) vendorDetail[12]))
				contact.add((String) vendorDetail[12]);
		}
		return contact;
	}

	private Map<String, Object> getNotificationContentIfExist(String sqlQuery, int primaryId, Date currentDate, int licenseExpiryDay, ComplianceType complianceType, 
			NotificationType notifyType) {
		Map<String, Object> contentMap = new HashMap<String, Object>();
		Date endDate=CommonUtil.getAdditionDate(currentDate,licenseExpiryDay);
		List<Object[]> notifyDetails = notificationService.getDetailsBasedOnNotifyType(sqlQuery, primaryId, currentDate, endDate, complianceType);
		StringBuilder emailContent = new StringBuilder();
		StringBuilder smsContent = new StringBuilder();
		int count = 1;
		if(!notifyDetails.isEmpty()){
			emailContent.append("<html><body><table border=\"1\">");
			if(ComplianceType.DRIVER.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType) 
					&& notifyType.getType().equalsIgnoreCase(ComplianceType.DRIVER.name()))){
				emailContent.append("<tr><td><b>Driver Id</b></td><td><b>Driver Name</b></td><td><b>Valid</b></td></tr>");
				smsContent.append("# DriverId/\tDriverName/\tValid\n");
			}else if(ComplianceType.VEHICLE.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType) 
					&& notifyType.getType().equalsIgnoreCase(ComplianceType.VEHICLE.name()))){
				emailContent.append("<tr><td><b>Vehicle Id</b></td><td><b>Vehicle Number</b></td><td><b>Valid</b></td></tr>");
				smsContent.append("# VehicleId/\tVehicleNumber/\tValid\n");
			}
			for(Object[] notifyDetail : notifyDetails){
				if(ComplianceType.DRIVER.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType) 
						&& notifyType.getType().equalsIgnoreCase(ComplianceType.DRIVER.name()))){
					emailContent.append("<tr>");
					emailContent.append("<td>");
					int id = notifyDetail [0] != null ? (int) notifyDetail[0] : 0;
					emailContent.append(id);
					smsContent.append(count+") "+id+"/\t");
					emailContent.append("</td>");
					emailContent.append("<td>");
					String fName = notifyDetail[1] != null ? notifyDetail[1].toString() : "";
					String lName = notifyDetail[2] != null ? notifyDetail[2].toString() : "";
					emailContent.append(fName +" "+lName);
					smsContent.append(fName +" "+lName+"/\t");
					emailContent.append("</td>");
					emailContent.append("<td>");
					String expire = notifyDetail[3] != null ? notifyDetail[3].toString() : "";
					emailContent.append(expire);
					smsContent.append(expire+"\t\n");
					emailContent.append("</td>");
					emailContent.append("</tr>");
				}else if(ComplianceType.VEHICLE.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType) 
						&& notifyType.getType().equalsIgnoreCase(ComplianceType.VEHICLE.name()))){
					emailContent.append("<tr>");
					emailContent.append("<td>");
					int id = notifyDetail [0] != null ? (int) notifyDetail[0] : 0;
					emailContent.append(id);
					smsContent.append(count+") "+id+"/\t");
					emailContent.append("</td>");
					emailContent.append("<td>");
					String vehicleNumber = notifyDetail[2] != null ? notifyDetail[2].toString() : "";
					emailContent.append(vehicleNumber);
					smsContent.append(vehicleNumber+"/\t");
					emailContent.append("</td>");
					emailContent.append("<td>");
					String expire = notifyDetail[3] != null ? notifyDetail[3].toString() : "";
					emailContent.append(expire);
					smsContent.append(expire+"\t\n");
					emailContent.append("</td>");
					emailContent.append("</tr>");
				}
				count++;
			}
			emailContent.append("</table></body></html>");
			smsContent.append("\n");
			contentMap.put("emailAttachment", emailContent.toString());
			contentMap.put("smsAttachment", smsContent.toString());
		}
		return contentMap;
	}

	private void triggerNotification(int branchId, String requestType, String emailId, String smsNumber,
			AuditType auditType, ComplianceType complianceType, NotificationType notificationType, Map<String, Object> notifyContent, int primaryId, String[] mailDetails) {
		log.info(complianceType.name()+" : Trigger Notification started .....");
		boolean smsFlag = false;
		boolean emailFlag = false;
		if(requestType != null){
			if("BOTH".equalsIgnoreCase(requestType) || "SMS".equalsIgnoreCase(requestType)){
				if(!StringUtils.isBlank(smsNumber)){
					try{
						log.info("Sending SMS to " +smsNumber+ " for Notification type "+ notificationType.name());
						String smsContent = notifyContent.get("smsContent") != null ? (String) notifyContent.get("smsContent")+"\n" : "";
						smsContent += notifyContent.get("smsAttachment") != null ? (String) notifyContent.get("smsAttachment") : "";
						smsContent += propertiesConfig.getTransportRegardsSms().replace("$SEPERATOR$", "\n");
						final String smsContentFinal = smsContent;
						log.info("SMS Content " + smsContent);
						Thread thread1 = new Thread(new Runnable() {
							@Override
							public void run() {
								SMSNotificationUtil.sendSMS(propertiesConfig.getSmsUrl(), smsNumber.toString(), smsContentFinal);
							}
						});
						thread1.start();
						smsFlag = true;
						log.info("SMS sent successfully to " +smsNumber+ " for Notification type "+ notificationType.name());
					}catch(Exception ex){
						log.info("Exception while sending SMS : "+ex.getMessage());
					}
				}
			}
			if("BOTH".equalsIgnoreCase(requestType) || "EMAIL".equalsIgnoreCase(requestType)){
				try{
					emailId = !StringUtils.isBlank(emailId) ? emailId : propertiesConfig.getEmailDefaultMail();
					final String emailIdFinal = emailId;
					log.info("Sending Email to " +emailId+ " for Notification type "+ notificationType.name());
					String emailContent = notifyContent.get("emailContent") != null ? (String) notifyContent.get("emailContent") : "";
					String emailAttachment = notifyContent.get("emailAttachment") != null ? (String) notifyContent.get("emailAttachment") : "";
					log.info("Email Content " + emailAttachment);
					Thread thread1 = new Thread(new Runnable() {
						@Override
						public void run() {
							mailNotification.sendEmail(propertiesConfig.getEmailSubject()+"-"+notificationType.getDocumentType()+" Expire", emailContent, emailAttachment, 
									propertiesConfig.getTransportRegardsEmail().replace("$SEPERATOR$", "\n"), emailIdFinal, mailDetails);
						}
					});
					thread1.start();
					emailFlag = true;
					log.info("Email sent successfully to " +emailId+ " for Notification type "+ notificationType.name());
				}catch(Exception ex){
					ex.printStackTrace();
					log.info("Exception while sending Email : ");
				}
			}
			EFmFmComplianceAuditPO complianceAudit = new EFmFmComplianceAuditPO();
			complianceAudit.setAuditDate(new Date());
			complianceAudit.setAuditType(auditType.name());
			complianceAudit.setBranchId(branchId);
			complianceAudit.setComplianceType(complianceType.name());
			complianceAudit.setNotificationType(notificationType.name());
			complianceAudit.setEmailFlag(emailFlag ? NotificationStatus.SENT.name() : NotificationStatus.NOTSENT.name());
			complianceAudit.setSmsFlag(smsFlag ? NotificationStatus.SENT.name() : NotificationStatus.NOTSENT.name());
			complianceAudit.setRequsetType(requestType);
			complianceAudit.setSmsSentTo(("BOTH".equalsIgnoreCase(requestType) || "SMS".equalsIgnoreCase(requestType) ? smsNumber : ""));
			complianceAudit.setEmailSentTo(("BOTH".equalsIgnoreCase(requestType) || "EMAIL".equalsIgnoreCase(requestType) ? emailId : ""));
			if(ComplianceType.VENDOR.equals(complianceType)){
				complianceAudit.setVendorId(primaryId);
			}
			notificationService.save(complianceAudit);
		}
	}

}
