package com.efmfm.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EFmFmComplianceNotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(EFmFmComplianceNotificationApplication.class, args);
	}
}
