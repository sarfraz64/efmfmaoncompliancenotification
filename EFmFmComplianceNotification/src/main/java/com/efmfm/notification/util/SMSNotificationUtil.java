package com.efmfm.notification.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;


public class SMSNotificationUtil {

	private static Logger logger = Logger.getLogger(SMSNotificationUtil.class);
	
	public static void sendSMS(String smsUrl,String mobileNo, String sms) {

		System.out.println("SMS ::" + sms);
		
		String text = null;
		try {
			text = URLEncoder.encode(sms, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url=smsUrl.replace("$mobileNo$", mobileNo).replace("$text$", text);
		final String USER_AGENT = "Mozilla/5.0";
		logger.info("Msg from National API" + url);
		URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = 0;
		BufferedReader in = null;
		String inputLine;
		StringBuffer response = new StringBuffer();
		try {
			responseCode = con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			logger.info("Message ValueFirstSMSMessager Response From GateWay: " + response.toString() + " for Mobile: "
					+ mobileNo + " : " + responseCode + " Message Text" + text);
		} else {
			logger.warn("Message ValueFirstSMSMessager Responce From Gateway GET request not worked for Mobile: "
					+ mobileNo + " : " + responseCode + " Message Text" + text);
		}
	}
}
