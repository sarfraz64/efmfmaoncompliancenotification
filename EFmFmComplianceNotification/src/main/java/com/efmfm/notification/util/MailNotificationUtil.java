package com.efmfm.notification.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Component;

@Component
public class MailNotificationUtil {
	public void sendEmail(String subject, String emailContent, String emailAttachment, String sentBy, String emailId, String[] mailDetails) {

		// Sender's email ID needs to be mentioned
		final String username = mailDetails[0];// change accordingly
		final String password = mailDetails[1];// change accordingly
		
		Properties props = new Properties();
		props.put("mail.smtp.host", mailDetails[2]);
		props.put("mail.smtp.auth", mailDetails[3]);
		props.put("mail.smtp.starttls.enable", mailDetails[4]);
		props.put("mail.smtp.port", mailDetails[5]);
		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));	
			String[] mailRecipient = emailId.split(",");
			Address[] ia = new InternetAddress[mailRecipient.length];
			int i = 0;
			for (String address : mailRecipient) {
				ia[i] = new InternetAddress(address);
			    i++;
			}
			message.addRecipients(RecipientType.TO, ia);
			message.setSubject(subject);
			message.setSentDate(new Date());
			//message.setContent(emailAttachment, "text/html; charset=utf-8");
			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(emailContent+"\n");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			String htmlText = emailAttachment;
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
		
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(sentBy);
			multipart.addBodyPart(messageBodyPart);
			
			message.setContent(multipart);
			// Send message
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
