package com.efmfm.notification.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class CommonUtil {
	
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	
	public static boolean isTriggerNotification(List<Object[]> lastExecutionList,
			int repeatAlert, Date currentDate) {
		Object[] lastExecution = null;
		if(!lastExecutionList.isEmpty())
			lastExecution = lastExecutionList.get(0);
		if(lastExecution != null && lastExecution[0] != null){
			Date lastExecutionDate = (Date) lastExecution[0];
			Date nextExecutionDate = CommonUtil.incrOrDecDaysByGivenDate(lastExecutionDate, repeatAlert);
			if (CommonUtil.getDatewithOutHHMMMMSS(currentDate)
					.compareTo(CommonUtil.getDatewithOutHHMMMMSS(nextExecutionDate)) == 0 || CommonUtil.getDatewithOutHHMMMMSS(nextExecutionDate)
					.compareTo(CommonUtil.getDatewithOutHHMMMMSS(currentDate)) < 0) {
				return true;
			}
		}
		return false;
	}
	
	public static List<String> getSplitedList(String str) {
		return Arrays.asList(str.split("\\s*,\\s*"));
	}
	
	public static Date getZoneDateTime(String timeZone) {
		Date today = new Date();
		Date zoneDate = null;
		DateFormat df = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		String zoneDateTime = df.format(today);
		try {
			zoneDate = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(zoneDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return zoneDate;
	}
	
	public static Date incrOrDecDaysByGivenDate(Date givenDate,int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(givenDate);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}
	
	public static Date getDatewithOutHHMMMMSS(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}
	
	public static Date getAdditionDate(Date timeZoneDateTime, int days) {
		 GregorianCalendar greCalendor = new GregorianCalendar();
		 greCalendor.setTime(timeZoneDateTime);
		 greCalendor.add(Calendar.DATE, +days);
		 return greCalendor.getTime();
		 
	}
	
	public enum AuditType{
		INTERNAL, VENDOR
	}
	
	public enum ComplianceType{
		DRIVER, VEHICLE, VENDOR
	}
	
	public enum NotificationType{
		LICENSE_EXPIRE("driver", "License"), MEDICAL_FITNESS("driver","Medical Fitness"), POLICE_VERIFICATION("driver","Police Verification")
		, DD_TRAINING("driver", "DD Training"), STATE_PERMIT_DUE("vehicle", "State Permit Due"), NATIONAL_PERMIT_DUE("vehicle", "NationalPermit Due"), POLLUTION_DUE("vehicle", "Pollution Due"), 
		INSURANCE_DUE("vehicle", "Insurance Due"), TAX_DUE("vehicle", "Tax Due"), VEHICLE_FITNESS("vehicle", "Vehicle Fitness");
		private String type;
		private String documentType;

		private NotificationType(String type, String documentType) {
			this.type = type;
			this.documentType = documentType;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDocumentType() {
			return documentType;
		}

		public void setDocumentType(String documentType) {
			this.documentType = documentType;
		}
		
	}
	
	public enum NotificationStatus{
		SENT, NOTSENT
	}
	
	public enum ContactType{
		SMS, EMAIL
	}

}
