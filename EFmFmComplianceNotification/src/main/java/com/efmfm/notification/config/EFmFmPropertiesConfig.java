package com.efmfm.notification.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="efmfm")
public class EFmFmPropertiesConfig {
	
	private String lastExecution;
	private String complianceBranchConfig;
	private String driverLicenseValid;
	private String driverPoliceValid;
	private String driverDdTraningValid;
	private String driverMedicalFitnessValid;
	private String vehicleStateValid;
	private String vehicleNationalValid;
	private String vehiclePollutionValid;
	private String vehicleInsuranceValid;
	private String vehicleTaxValid;
	private String vehicleFitnessValid;
	private String lastExecutionVendor;
	private String vendorCompConfigBranch;
	private String uniqueVendor;
	private String vendorLicenseValid;
	private String vendorPoliceValid;
	private String vendorMedicalFitnessValid;
	private String vendorDdTraningValid;
	private String vendorStatePermitValid;
	private String vendorNationalPermitValid;
	private String vendorPollutionValid;
	private String vendorInsuranceValid;
	private String vendorTaxValid;
	private String vendorFitnessValid;
	private String smsUrl;
	private String transportVendorEmail;
	private String transportVendorSms;
	private String transportVehicleDriverSms;
	private String transportVehicleDriverEmail;
	private String transportRegardsSms;
	private String transportRegardsEmail;
	private String emailSubject;
	private String emailDefaultMail;
	private String combinedFacility;
	
	public String getLastExecution() {
		return lastExecution;
	}

	public void setLastExecution(String lastExecution) {
		this.lastExecution = lastExecution;
	}

	public String getDriverPoliceValid() {
		return driverPoliceValid;
	}

	public void setDriverPoliceValid(String driverPoliceValid) {
		this.driverPoliceValid = driverPoliceValid;
	}

	public String getComplianceBranchConfig() {
		return complianceBranchConfig;
	}

	public void setComplianceBranchConfig(String complianceBranchConfig) {
		this.complianceBranchConfig = complianceBranchConfig;
	}

	public String getDriverLicenseValid() {
		return driverLicenseValid;
	}

	public void setDriverLicenseValid(String driverLicenseValid) {
		this.driverLicenseValid = driverLicenseValid;
	}

	public String getDriverDdTraningValid() {
		return driverDdTraningValid;
	}

	public void setDriverDdTraningValid(String driverDdTraningValid) {
		this.driverDdTraningValid = driverDdTraningValid;
	}

	public String getVehicleStateValid() {
		return vehicleStateValid;
	}

	public void setVehicleStateValid(String vehicleStateValid) {
		this.vehicleStateValid = vehicleStateValid;
	}

	public String getVehicleNationalValid() {
		return vehicleNationalValid;
	}

	public void setVehicleNationalValid(String vehicleNationalValid) {
		this.vehicleNationalValid = vehicleNationalValid;
	}

	public String getVehiclePollutionValid() {
		return vehiclePollutionValid;
	}

	public void setVehiclePollutionValid(String vehiclePollutionValid) {
		this.vehiclePollutionValid = vehiclePollutionValid;
	}

	public String getVehicleInsuranceValid() {
		return vehicleInsuranceValid;
	}

	public void setVehicleInsuranceValid(String vehicleInsuranceValid) {
		this.vehicleInsuranceValid = vehicleInsuranceValid;
	}

	public String getVehicleTaxValid() {
		return vehicleTaxValid;
	}

	public void setVehicleTaxValid(String vehicleTaxValid) {
		this.vehicleTaxValid = vehicleTaxValid;
	}

	public String getVehicleFitnessValid() {
		return vehicleFitnessValid;
	}

	public void setVehicleFitnessValid(String vehicleFitnessValid) {
		this.vehicleFitnessValid = vehicleFitnessValid;
	}

	public String getLastExecutionVendor() {
		return lastExecutionVendor;
	}

	public void setLastExecutionVendor(String lastExecutionVendor) {
		this.lastExecutionVendor = lastExecutionVendor;
	}

	public String getVendorCompConfigBranch() {
		return vendorCompConfigBranch;
	}

	public void setVendorCompConfigBranch(String vendorCompConfigBranch) {
		this.vendorCompConfigBranch = vendorCompConfigBranch;
	}

	public String getUniqueVendor() {
		return uniqueVendor;
	}

	public void setUniqueVendor(String uniqueVendor) {
		this.uniqueVendor = uniqueVendor;
	}

	public String getVendorLicenseValid() {
		return vendorLicenseValid;
	}

	public void setVendorLicenseValid(String vendorLicenseValid) {
		this.vendorLicenseValid = vendorLicenseValid;
	}

	public String getVendorPoliceValid() {
		return vendorPoliceValid;
	}

	public void setVendorPoliceValid(String vendorPoliceValid) {
		this.vendorPoliceValid = vendorPoliceValid;
	}

	public String getVendorMedicalFitnessValid() {
		return vendorMedicalFitnessValid;
	}

	public void setVendorMedicalFitnessValid(String vendorMedicalFitnessValid) {
		this.vendorMedicalFitnessValid = vendorMedicalFitnessValid;
	}

	public String getVendorDdTraningValid() {
		return vendorDdTraningValid;
	}

	public void setVendorDdTraningValid(String vendorDdTraningValid) {
		this.vendorDdTraningValid = vendorDdTraningValid;
	}

	public String getVendorStatePermitValid() {
		return vendorStatePermitValid;
	}

	public void setVendorStatePermitValid(String vendorStatePermitValid) {
		this.vendorStatePermitValid = vendorStatePermitValid;
	}

	public String getVendorNationalPermitValid() {
		return vendorNationalPermitValid;
	}

	public void setVendorNationalPermitValid(String vendorNationalPermitValid) {
		this.vendorNationalPermitValid = vendorNationalPermitValid;
	}

	public String getVendorPollutionValid() {
		return vendorPollutionValid;
	}

	public void setVendorPollutionValid(String vendorPollutionValid) {
		this.vendorPollutionValid = vendorPollutionValid;
	}

	public String getVendorInsuranceValid() {
		return vendorInsuranceValid;
	}

	public void setVendorInsuranceValid(String vendorInsuranceValid) {
		this.vendorInsuranceValid = vendorInsuranceValid;
	}

	public String getVendorTaxValid() {
		return vendorTaxValid;
	}

	public void setVendorTaxValid(String vendorTaxValid) {
		this.vendorTaxValid = vendorTaxValid;
	}

	public String getVendorFitnessValid() {
		return vendorFitnessValid;
	}

	public void setVendorFitnessValid(String vendorFitnessValid) {
		this.vendorFitnessValid = vendorFitnessValid;
	}

	public String getSmsUrl() {
		return smsUrl;
	}

	public void setSmsUrl(String smsUrl) {
		this.smsUrl = smsUrl;
	}

	public String getTransportVendorEmail() {
		return transportVendorEmail;
	}

	public void setTransportVendorEmail(String transportVendorEmail) {
		this.transportVendorEmail = transportVendorEmail;
	}

	public String getTransportVendorSms() {
		return transportVendorSms;
	}

	public void setTransportVendorSms(String transportVendorSms) {
		this.transportVendorSms = transportVendorSms;
	}

	public String getTransportVehicleDriverSms() {
		return transportVehicleDriverSms;
	}

	public void setTransportVehicleDriverSms(String transportVehicleDriverSms) {
		this.transportVehicleDriverSms = transportVehicleDriverSms;
	}

	public String getTransportVehicleDriverEmail() {
		return transportVehicleDriverEmail;
	}

	public void setTransportVehicleDriverEmail(String transportVehicleDriverEmail) {
		this.transportVehicleDriverEmail = transportVehicleDriverEmail;
	}

	public String getTransportRegardsSms() {
		return transportRegardsSms;
	}

	public void setTransportRegardsSms(String transportRegardsSms) {
		this.transportRegardsSms = transportRegardsSms;
	}

	public String getTransportRegardsEmail() {
		return transportRegardsEmail;
	}

	public void setTransportRegardsEmail(String transportRegardsEmail) {
		this.transportRegardsEmail = transportRegardsEmail;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailDefaultMail() {
		return emailDefaultMail;
	}

	public void setEmailDefaultMail(String emailDefaultMail) {
		this.emailDefaultMail = emailDefaultMail;
	}

	public String getCombinedFacility() {
		return combinedFacility;
	}

	public void setCombinedFacility(String combinedFacility) {
		this.combinedFacility = combinedFacility;
	}

	public String getDriverMedicalFitnessValid() {
		return driverMedicalFitnessValid;
	}

	public void setDriverMedicalFitnessValid(String driverMedicalFitnessValid) {
		this.driverMedicalFitnessValid = driverMedicalFitnessValid;
	}

	@Override
	public String toString() {
		return "EFmFmPropertiesConfig [lastExecution=" + lastExecution + ", complianceBranchConfig="
				+ complianceBranchConfig + ", driverLicenseValid=" + driverLicenseValid + ", driverPoliceValid="
				+ driverPoliceValid + ", driverDdTraningValid=" + driverDdTraningValid + ", driverMedicalFitnessValid="
				+ driverMedicalFitnessValid + ", vehicleStateValid=" + vehicleStateValid + ", vehicleNationalValid="
				+ vehicleNationalValid + ", vehiclePollutionValid=" + vehiclePollutionValid + ", vehicleInsuranceValid="
				+ vehicleInsuranceValid + ", vehicleTaxValid=" + vehicleTaxValid + ", vehicleFitnessValid="
				+ vehicleFitnessValid + ", lastExecutionVendor=" + lastExecutionVendor + ", vendorCompConfigBranch="
				+ vendorCompConfigBranch + ", uniqueVendor=" + uniqueVendor + ", vendorLicenseValid="
				+ vendorLicenseValid + ", vendorPoliceValid=" + vendorPoliceValid + ", vendorMedicalFitnessValid="
				+ vendorMedicalFitnessValid + ", vendorDdTraningValid=" + vendorDdTraningValid
				+ ", vendorStatePermitValid=" + vendorStatePermitValid + ", vendorNationalPermitValid="
				+ vendorNationalPermitValid + ", vendorPollutionValid=" + vendorPollutionValid
				+ ", vendorInsuranceValid=" + vendorInsuranceValid + ", vendorTaxValid=" + vendorTaxValid
				+ ", vendorFitnessValid=" + vendorFitnessValid + ", smsUrl=" + smsUrl + ", transportVendorEmail="
				+ transportVendorEmail + ", transportVendorSms=" + transportVendorSms + ", transportVehicleDriverSms="
				+ transportVehicleDriverSms + ", transportVehicleDriverEmail=" + transportVehicleDriverEmail
				+ ", transportRegardsSms=" + transportRegardsSms + ", transportRegardsEmail=" + transportRegardsEmail
				+ ", emailSubject=" + emailSubject + ", emailDefaultMail=" + emailDefaultMail + ", combinedFacility="
				+ combinedFacility + "]";
	}
	
}
