package com.efmfm.notification.service;

import java.util.Date;
import java.util.List;

import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.util.CommonUtil.ComplianceType;

public interface EFmFmNotificationService {

	public List<Object[]> getAllBranchDetails(String query, List<String> combinedFacility);

	public List<Object[]> getLastExecutionByNotifyType(String lastExecutionQuery, int branchId, String auditType,
			String complianceType, String notificationType, int primaryId);

	public List<Object[]> getDetailsBasedOnNotifyType(String sqlQuery, int branchId, Date currentDate, Date endDate, ComplianceType complianceType);

	public void save(EFmFmComplianceAuditPO complianceAudit);

	public List<Object[]> getVendorDetailsByBranchId(String sqlQuery, int branchId);
	
		
	

}
