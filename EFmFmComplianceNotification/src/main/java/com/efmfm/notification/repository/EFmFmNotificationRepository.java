package com.efmfm.notification.repository;

import java.util.Date;
import java.util.List;

import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.util.CommonUtil.ComplianceType;

public interface EFmFmNotificationRepository {

	List<Object[]> getAllBranchDetails(String query, List<String> combinedFacility);

	List<Object[]> getLastExecutionByNotifyType(String lastExecutionQuery, int branchId, String auditType,
			String complianceType, String notificationType, int vendorId);

	List<Object[]> getDetailsBasedOnNotifyType(String sqlQuery, int branchId, Date startDate, Date endDate, ComplianceType complianceType);

	void save(EFmFmComplianceAuditPO complianceAudit);

	List<Object[]> getVendorDetailsByBranchId(String sqlQuery, int branchId);

	
}
