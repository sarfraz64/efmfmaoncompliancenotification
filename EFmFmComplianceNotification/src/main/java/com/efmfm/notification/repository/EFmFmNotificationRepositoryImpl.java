package com.efmfm.notification.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.util.CommonUtil.ComplianceType;

@Repository
@SuppressWarnings("unchecked")
public class EFmFmNotificationRepositoryImpl implements EFmFmNotificationRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Object[]> getAllBranchDetails(String sqlQuery, List<String> combinedFacility) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchIds", combinedFacility);
		return query.getResultList();
	}

	@Override
	public List<Object[]> getLastExecutionByNotifyType(String sqlQuery, int branchId, String auditType,
			String complianceType, String notificationType, int vendorId) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchId", branchId).setParameter("auditType", auditType)
				.setParameter("complianceType", complianceType).setParameter("notificationType", notificationType);
		if(ComplianceType.VENDOR.name().equalsIgnoreCase(complianceType) && vendorId!=0){
			query.setParameter("vendorId", vendorId);
		}
		return query.getResultList();
	}

	@Override
	public List<Object[]> getDetailsBasedOnNotifyType(String sqlQuery, int primaryId, Date startDate, Date endDate, ComplianceType complianceType) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("endDate", endDate);
		if(ComplianceType.VENDOR.equals(complianceType)){
			query.setParameter("vendorId", primaryId);
		}else{
			query.setParameter("branchId", primaryId);
		}
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void save(EFmFmComplianceAuditPO complianceAudit) {
		this.em.persist(complianceAudit);
	}

	@Override
	public List<Object[]> getVendorDetailsByBranchId(String sqlQuery, int branchId) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchId", branchId);
		return query.getResultList();
	}

}
